#!/bin/bash
#
# Script d'installation du socle Bash
#
# 21/07/2017 - Doug Le Tough / Tetalab
# 

########################################################################
#
# Fonctions
#
########################################################################

function exit_on_error
{
  ERROR="$@"
  echo -e "\033[91m*** ${ERROR} *** \033[0m"
  exit 1
}

function continue_on_ok
{
  echo -e "\033[92m[OK]\033[0m"
}

########################################################################
#
# Contrôles
#
########################################################################

if [ ! $(id -u) -eq 0 ]; then
  exit_on_error "Ce script doit être exécuté par l'utilisateur root"
fi

PKG_DIR=$(dirname $0)
PKG_INSTALL_CONF=${PKG_DIR}/conf/install_init.conf
PKG_SOCLE_INIT_CONF=${PKG_DIR}/conf/shell-env/profile_init.env

if [ ! -r ${PKG_INSTALL_CONF} ] || [ ! -r ${PKG_SOCLE_INIT_CONF}  ] ; then
  exit_on_error "Erreur lors de l'initialisation de l'environnement d'installation. Vérifier les permissions sur les fichiers ${PKG_INSTALL_CONF} et ${PKG_SOCLE_INIT_CONF} "
fi

########################################################################
#
# Main
#
########################################################################

source ${PKG_INSTALL_CONF}
source ${PKG_SOCLE_INIT_CONF} 2>/dev/null

echo -e "\033[93mInstallation du socle Bash:\033[0m"
echo -e "\033[93m * Répertoire racine: ${NC_EXPL_ROOT}\033[0m"
echo -e "\033[93m * Création des répertoires:\033[0m"

# Création des répertoires de destination
for DIR in ${NC_EXPL_LIB} ${NC_EXPL_BIN} ${NC_EXPL_LOG} ${NC_EXPL_HST} ${NC_EXPL_CPT} ${NC_EXPL_TMP} ${NC_EXPL_DAT} ${NC_EXPL_MOD} ${NC_EXPL_CFG} ${NC_EXPL_MSG} ${NC_EXPL_MAN};
do
  echo -en "    * ${DIR} "
  sudo mkdir -p ${DIR};
  RET_VAL=$?
  if [ ! ${RET_VAL} -eq 0 ]; then
    exit_on_error "Erreur lors de la création des répertoires"
  fi
  continue_on_ok
done

# Installation des fichiers de profile
echo -en "\033[93m * Création du fichier de profile dans ${PROFILE_DIR}\033[0m "

cat << EOF > ${PROFILE_DIR}/${PROFILE_FILE}
NC_EXPL_ROOT=${NC_EXPL_ROOT}
export NC_EXPL_ROOT
source \${NC_EXPL_ROOT}/conf/shell-env/profile_init.env
EOF
RET_VAL=$?
if [ ! ${RET_VAL} -eq 0 ]; then
  exit_on_error "Erreur lors de la création du fichier de profile"
fi
chmod 755 ${PROFILE_DIR}/${PROFILE_FILE}
RET_VAL=$?
if [ ! ${RET_VAL} -eq 0 ]; then
  exit_on_error "Erreur lors de l'application des permissions sur ${PROFILE_DIR}/${PROFILE_FILE}"
fi
continue_on_ok

# Installation des fichiers de configuration
echo -en "\033[93m * Copie des fichiers de configuration dans ${NC_EXPL_CFG}\033[0m "
cp -R ${PKG_DIR}/conf/* ${NC_EXPL_CFG}
RET_VAL=$?
if [ ! ${RET_VAL} -eq 0 ]; then
  exit_on_error "Erreur lors de la copie des fichiers de configuration"
fi
continue_on_ok

# Installation des bibliothèques
echo -en "\033[93m * Copie des bibliothèques dans ${NC_EXPL_LIB}\033[0m "
cp -R ${PKG_DIR}/lib/* ${NC_EXPL_LIB}
RET_VAL=$?
if [ ! ${RET_VAL} -eq 0 ]; then
  exit_on_error "Erreur lors de la copie des bibliothèques"
fi
continue_on_ok

# Installation des scripts
SCRIPTS_NUM=$(find ${PKG_DIR}/bin/ -type f | wc -l)
if [ ! ${SCRIPTS_NUM} -eq 0 ]; then
  echo -en "\033[93m * Copie des scripts dans ${NC_EXPL_BIN}\033[0m "
  cp -R ${PKG_DIR}/bin/* ${NC_EXPL_BIN}
  RET_VAL=$?
  if [ ! ${RET_VAL} -eq 0 ]; then
    exit_on_error "Erreur lors de la copie des scripts"
  fi
  continue_on_ok
fi

# Installation des fichiers msg relatifs aux scripts
MSGS_NUM=$(find ${PKG_DIR}/msg/ -type f | wc -l)
if [ ! ${MSGS_NUM} -eq 0 ]; then
  echo -en "\033[93m * Copie des fichiers .msg dans ${NC_EXPL_MSG}\033[0m "
  cp -R ${PKG_DIR}/msg/* ${NC_EXPL_MSG}
  RET_VAL=$?
  if [ ! ${RET_VAL} -eq 0 ]; then
    exit_on_error "Erreur lors de la copie des fichiers .msg"
  fi
  continue_on_ok
fi

# Installation des fichiers modèles
TEMPLATES_NUM=$(find ${PKG_DIR}/mod/ -type f | wc -l)
if [ ! ${TEMPLATES_NUM} -eq 0 ]; then
  echo -en "\033[93m * Copie des fichiers modèles dans ${NC_EXPL_MOD}\033[0m "
  cp -R ${PKG_DIR}/mod/* ${NC_EXPL_MOD}
  RET_VAL=$?
  if [ ! ${RET_VAL} -eq 0 ]; then
    exit_on_error "Erreur lors de la copie des fichiers modèles"
  fi
  continue_on_ok
fi

# Gestion des droits et permissions
echo -en "\033[93m * Application des droits sur les répertoires:\033[0m "
chown -R ${ADMIN_USER}:${ADMIN_GROUP} ${NC_EXPL_ROOT}
RET_VAL=$?
if [ ! ${RET_VAL} -eq 0 ]; then
  exit_on_error "Erreur lors l'application des droits"
fi
continue_on_ok

echo -e "\033[93m * Application des permissions sur les répertoires:\033[0m "
for DIR in cpt bin conf dat lib man mod msg
do
  echo -en "    * ${NC_EXPL_ROOT}/${DIR} "
  chmod -R 555 ${NC_EXPL_ROOT}/${DIR}
  RET_VAL=$?
  if [ ! ${RET_VAL} -eq 0 ]; then
    exit_on_error "Erreur lors l'application des droits sur ${NC_EXPL_ROOT}/${DIR}"
  fi
  continue_on_ok
done

for DIR in cpt hist log tmp
do
  echo -en "    * ${NC_EXPL_ROOT}/${DIR} "
  chmod -R 775 ${NC_EXPL_ROOT}/${DIR}
  RET_VAL=$?
  if [ ! ${RET_VAL} -eq 0 ]; then
    exit_on_error "Erreur lors l'application des droits sur ${NC_EXPL_ROOT}/${DIR}"
  fi
  continue_on_ok
done
