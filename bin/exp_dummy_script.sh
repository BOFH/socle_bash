#!/bin/bash
#------------------------------------------------------------------------------------------------------------------
# Script         : exp_dummy_script.sh
# Auteur         : Doug Le Tough
# Date           : 14-08-2017
# Version        : 1.2.0
# Objet          : Script d'exemple qui liste les VM définies dans son fichier de configuration
#
#------------------------------------------------------------------------------------------------------------------
# Mise a jour :
#
# 1.0.0  22/07/2017   - Doug Le Tough   - Cre : Mise en production
# 1.2.0  14/08/2017   - Doug Le Tough   - Evo : Ajout de l'option -gen_error
#
#------------------------------------------------------------------------------------------------------------------
# Dependances : Aucune
#
#------------------------------------------------------------------------------------------------------------------
# Liste des options et arguments:
#
# @OPT: f:list_vm:list_vm:1/1:true:::
# @OPT: f:gen_error:gen_error:1/1:false:::
# @OPT: f:argv:argv:1/::::
#
#------------------------------------------------------------------------------------------------------------------
# Liste des paramètres de configuration:
#
# DUMMY_VAR="Variable bidon pour test"
# TETALAB_VM[0]=sousetsukenlocal.tetalab.org
# TETALAB_VM[1]=jimmy.local.tetalab.org
# TETALAB_VM[2]=billy.local.tetalab.org
# TETALAB_VM[3]=marian.local.tetalab.org
# TETALAB_VM[4]=sonny.local.tetalab.org
#
#------------------------------------------------------------------------------------------------------------------
# Liste des erreurs:
#
# 2 | erreur | stop | ${ERROR}
# 3 | erreur | exec | rm_temp_dir
#
#------------------------------------------------------------------------------------------------------------------
##
## Syntaxe :
## --------
##
##  prompt> exp_dummy_script.sh [-list_vm false] [ -gen_error true ] [-argv arg1 arg2 ...]
##
##  ex:
##       exp_dummy_script.sh -list_vm false -gen_error true -argv plop plip plap
##
## Prérequis:
## ----------
## - Le script doit être executé par l'utilisateur asr
##
## Fonctionnement:
## ---------------
## Affiche sa configuration et liste les VM listées dans son fichier de configuration (TETALAB_VM[*]).
##
## Si l'option -list_vm est positionnée à false, le script ne fait qu'afficher sa configuration.
## Par défaut list_vm vaut true.
##
## Si l'option -gen_error  est positionnée à true, le script génèrera une erreur. Le but de cette option
## étant d'illustrer la gestion des erreurs. Par défaut gen_error vaut false.
##
#------------------------------------------------------------------------------------------------------------------
#                     Initialisation de l'environement
#------------------------------------------------------------------------------------------------------------------

if [ "${USER}" != "asr" ]; then
  ERROR="Seul l'utilisateur asr peut utiliser ce script"
  echo -e "\033[91m${ERROR}\033[0m"
  exit 1
fi

source /etc/profile.d/tetalab.sh

if [ ! -f ${NC_EXPL_CFG}/init.conf ]; then
  echo "Le fichier d'initialisation du socle \${NC_EXPL_CFG}/init.conf n'existe pas !"
  echo "Arrêt du script par sécurité"
  exit 250
fi

source ${NC_EXPL_CFG}/init.conf

#------------------------------------------------------------------------------------------------------------------
#  Fonctions
#------------------------------------------------------------------------------------------------------------------

function check_config
{
  fct_message "Vérification de la configuration:" -color jaune
  if [ ! -r ${SH_FICCFG} ]; then
    ERROR="Fichier de configuration ${SH_FICCFG} absent ou illisible"
    fct_erreur 2
  fi
  if [ ${#DUMMY_VAR} -eq 0 ]; then
    ERROR="Paramètre manquant ou vide dans le fichier de configuration: DUMMY_VAR"
    fct_erreur 2
  fi
  if [ ${#TETALAB_VM[*]} -eq 0 ]; then
    ERROR="Aucune VM définie dans le fichier de configuration"
    fct_erreur 2
  fi
  fct_message "  * DUMMY_VAR: ${DUMMY_VAR}"
  INDEX=0
  for VM in ${TETALAB_VM[@]}
  do
    fct_message "  * VM[${INDEX}]: ${VM}"
    INDEX=$(( INDEX+1 ))
  done
}

function mk_temp_dir {
  TMP_DIR="${NC_EXPL_TMP}/${SH_PROG}_${SH_SESSION_ID}"
  fct_message "Création du répertoire temporaire ${TMP_DIR}" -color jaune
  mkdir -p ${TMP_DIR}
  RET_VAL=$?
  if [ ! ${RET_VAL} -eq 0 ]; then
    ERROR="Erreur lors de la création du répertoire"
    fct_erreur 2
  fi
}

function rm_temp_dir {
  fct_message "Suppression du répertoire temporaire ${TMP_DIR}" -color jaune
  rm -Rf ${TMP_DIR}
  RET_VAL=$?
  if [ ! ${RET_VAL} -eq 0 ]; then
    ERROR="Erreur lors de la suppression du répertoire"
    fct_erreur 2
  fi
}

function gen_error {
  if [ "${gen_error}" == "true" ]; then
    fct_message "Génération d'une erreur volontaire:" -color jaune
    ERROR="GURU MEDITATION #00001337.00713705"
    fct_message "  - Erreur: ${ERROR}" -color vert
    # On supprime le répertoire temporaire en passant
    # par l'erreur 3
    fct_erreur 3
    # On quitte sur l'affichage de l'erreur et un exit code 2
    fct_erreur 2
  fi
}

function list_vm
{
  if [ "${list_vm}" == "true" ]; then
    fct_message "Liste des VM définies dans ${SH_FICCFG}:" -color jaune
    INDEX=0
    for VM in ${TETALAB_VM[@]}
    do
      fct_message "  * VM[${INDEX}]: ${VM}"
      INDEX=$(( INDEX+1 ))
    done
  fi
}

function print_argv
{
  if [ ${#argv} -gt 0 ]; then
    fct_message "Arguments optionnels:"
    for ARG in ${argv[@]}
    do
      fct_message "  - ${ARG}"
    done
  fi
}

function cat_piped_data
{
  fct_message "Données pipées:"
  cat > ${TMP_DIR}/${SH_PROG}_${SH_SESSION_ID}
  cat ${TMP_DIR}/${SH_PROG}_${SH_SESSION_ID}
}

#------------------------------------------------------------------------------------------------------------------
# Traitement
#------------------------------------------------------------------------------------------------------------------

check_config
mk_temp_dir
list_vm
print_argv
gen_error
cat_piped_data
rm_temp_dir
fct_erreur 0
