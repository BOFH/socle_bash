# Historique

Le **socle Bash** est **une version allégée** d'un ensemble de bibliothèques et fonctions
écrit en grande partie par **Emmanuel Confrère**, émminent DBA Calédonien qui nous a
autorisé à l'utiliser au sein du Tetalab, et dont l'utilité est d'assurer la cohérence
 des scripts qui s'appuient dessus.

Ce **socle Bash** est fiable et robuste. Il peut sans aucun doute être utilisé en production
à la condition que l'ensemble des conseils listés dans la documentation soient respectés.

Entre autres fonctionalités ce **socle Bash** permet aux scripts qui s'appuient dessus:

* La gestion de l'historique d'exécution
* La gestion des logs
* La gestion des fichiers temporaires
* La gestion des options et arguments
* La gestion des erreurs
* La standadisation de l'affichage
* etc

# Installation

L'installlation du script bash est simple et nécessite les droits **root**.

* Éditez le fichier de configuration **./conf/install_init.conf** de manière à ce
que les variables qui y sont définies correspondent à vos besoins.

          # La variable d'environnement qui définira l'emplacement
          # de l'ensemble du socle Bash
          NC_EXPL_ROOT=/opt/tetalab
          export NC_EXPL_ROOT

          # Le repertoire d'installation des fichiers de profiles système
          PROFILE_DIR=/etc/profile.d

          # Le nom du fichier de profile système à créer
          PROFILE_FILE=tetalab.sh

          # Utilisateur administrateur de l'infrastructure
          ADMIN_USER=asr

          # Groupe des utilisateurs habilités à utiliser le socle
          ADMIN_GROUP=asr


* Lancez l'installation en tant qu'utilisateur **root** (ou sudo):

        sudo ./install.sh

# Documentation

* **Sommaire**
  * I - **Arborescence du socle**
  * II - **Variables d'environement**
      * 1 - Variables globales
      * 2 - Variables de session
  * III - **Utilisation du socle bash**
      * 1 - Cartouche
        * 1.1 Élements du cartouche
        * 1.2 Versionnement sémantique
          * 1.2.1 Usage
          * 1.2.2 Format
          * 1.2.3 Sémantique
      * 2 - Initialisation du socle
      * 3 - Structure d'un script
      * 4 - Actions realisées au lancement d'un script
  * IV  - **Nomenclature**
      * 1 - Fichiers temporaires
      * 2 - Nom des exécutables
  * V - **Description et usage des bibliothèques**
      * 1 - fct\_message
      * 2 - fct\_affiche\_ligne
      * 3 - fct\_erreur
      * 4 - fct\_unique\_exec
      * 5 - fct\_libere\_exec
      * 6 - fct\_usage
      * 7 - fct\_rotation
      * 8 - fctpath
      * 9 - fct\_params


## I - Arborescence du socle

La racine des dossiers utilisée par le **socle Bash** est définie par la variable d'environement **NC\_EXPL\_ROOT**.
Cette dernière est initialisée à la connexion par l'appel au **fichier de profile** (voir installation)

Cette arborescence ainsi que les variables d'environement qui en découlent sont utilisées par tous scripts Bash

Sous l'arborescence **${NC\_EXPL\_ROOT}** on trouve les dossiers suivants :

    +-----------------+-----------------------------------------------------------------------------------------------+
    | Dossier         |                                   Description                                                 |
    +-----------------+-----------------------------------------------------------------------------------------------+
    |  {Racine}/bin   | Contient les binaires (scripts Bash)                                                          |
    |  {Racine}/conf  | Contient les fichiers de configuration des scripts, ainsi que du socle                        |
    |  {Racine}/cpt   | Contient les fichiers de comtpe-rendus génerés par les script (purge auto, 8 jours par défaut)|
    |  {Racine}/dat   | Contient les fichiers de données gérés par les scripts                                        |
    |  {Racine}/hist  | Contient l'historique d'exécution des scripts (pas de purge)                                  |
    |  {Racine}/lib   | Contient les bibliothèques chargées par le socle                                              |
    |  {Racine}/log   | Contient les fichiers de logs d exécution des scripts (purge auto , 8 jours par défaut )      |
    |  {Racine}/man   | Contient les fichiers d'aide des scripts pour l'utilitaire "man"                              |
    |  {Racine}/mod   | Contient les fichiers modèles (templates) à usage des scripts                                 |
    |  {Racine}/msg   | Contient les fichiers de messages des scripts utilisés par la bibliothèque "fct_erreur"       |
    |  {Racine}/tmp   | Contient les fichiers temporaires des scripts                                                 |
    +-----------------+-----------------------------------------------------------------------------------------------+



**Note:** Sous chaque dossier vous trouverez un fichier texte lisezmoi.txt decrivant l'usage du répertoire.


## II  - Variables d'environement

Le socle utilise deux groupes de variables. Le premier dit "global" le second "de session".

Sauf si explicite, les variables globales sont définies et exportées à la connexion. Elles sont donc utilisables 
par tous les scripts, mais elles ne sont pas modifiables.

Les variables de session sont définies à l'execution d'un script et lui sont propres. 
Cependent certaines d'entre elles sont transmises aux scripts enfants:

* SH\_SESSION\_ID
* SH\_FICLOG
* SH\_FICCPT
* SH\_RETENTION\_CPT
* SH\_RETENTION\_LOG
* SH\_NIV\_DEBUG
* SH\_SILENCE
* SH\_AFF\_SID

Les variables de session sont modifiable à 2 niveaux. De facon globale dans le fichier **init.conf** ou pour un script
particulier dans le fichier **${NC_EXPL_CFG}/<Nom_du_script>.conf**.

La valeur définie dans **init.conf** sera utlisée sauf si elle a été surchargée dans le fichier de configuration du
script.

Il y a cependant une spécificite pour **SH_RETENTION_CPT** et **SH_RETENTION_LOG**:

ces variables sont définies une et une seule fois lors de la première initialisation. Ce qui signifie qu'un
script qui aurait surchargé ces variables ignorera l'initialisation de ces variables si il est appelé par un
autre script.


### 1 - Variables globales générales

    +-------------------+----------------------------+
    | Nom               | Usage                      |
    +-------------------+----------------------------+
    | NC_EXPL_ROOT      | Racine      /opt/expl      |
    | EXPL_ROOT         | Alias pour NC_EXPL_ROOT    |
    | NC_EXPL_BIN       | Pointe vers {Racine}/bin   |
    | NC_EXPL_CFG       | Pointe vers {Racine}/conf  |
    | NC_EXPL_CPT       | Pointe vers {Racine}/cpt   |
    | NC_EXPL_DAT       | Pointe vers {Racine}/dat   |
    | NC_EXPL_HST       | Pointe vers {Racine}/hist  |
    | NC_EXPL_LIB       | Pointe vers {Racine}/lib   |
    | NC_EXPL_LOG       | Pointe vers {Racine}/log   |
    | NC_EXPL_MAN       | Pointe vers {Racine}/man   |
    | NC_EXPL_MOD       | Pointe vers {Racine}/mod   |
    | NC_EXPL_MSG       | Pointe vers {Racine}/msg   |
    | NC_EXPL_TMP       | Pointe vers {Racine}/tmp   |
    | RUBYLIB           | vaut ${NC_EXPL_LIB}        |
    | NC_EXPL_SRVBATCH  | Nom du serveur Batch       |
    +-------------------+----------------------------+



Les variables **NC\_EXPL\_ROOT**, **EXPL\_ROOT** et **NC\_EXPL\_SRVBATCH** sont
définies dans **le fichier de profile** (voir installation)

Les autres variables **NC\_\*** sont définies dans le fichier **profile\_init.env**


### 2 - Variables de session

Cette liste n'est pas exhaustive, seules les principales variables sont citées.

    +--------------------+-------------------------------------------------------------------------------+
    | Nom                | Usage                                                                         |
    +--------------------+-------------------------------------------------------------------------------+
    | SH_SESSION_ID      | Numero unique d'exécution : <TimeStamp>_<PID>                                 |
    | SH_SILENCE         | Active ou non l'affichage des messages via fct_message. Par defaut vaut "non" |
    | SH_PROG            | Nom du script                                                                 |
    | SH_LOGUSER         | Compte initial de connexion au serveur                                        |
    | SH_EXECUSER        | Compte courant exécutant le script                                            |
    | SH_DIR             | Dossier hébergeant le script                                                  |
    | SH_PROG_VERSION    | Version du script                                                             |
    | SH_FICTRC          | Pointe vers le fichier ${NC_EXPL_HST}/<Nom_du_script>.hist                    |
    | SH_FICMSG          | Pointe vers le fichier ${NC_EXPL_MSG}/<Nom_du_script>.msg                     |
    | SH_FICCFG          | Pointe vers le fichier ${NC_EXPL_CFG}/<Nom_du_script>.conf                    |
    | SH_FICSEM          | Pointe vers le fichier /dev/shm/<Nom_du_script>.sem                           |
    | SH_FICLOG          | Pointe vers le fichier ${NC_EXPL_LOG}/<Nom_du_script>.log                     |
    | SH_FICCPT          | Pointe vers le fichier ${NC_EXPL_CPT}/<Nom_du_script>.cpt                     |
    | SH_NIV_DEBUG       | Vaut 0 par défaut. Niveau d'affichage dans les fichiers de traces             |
    | SH_RETENTION_CPT   | Vaut 8 par défaut. Duree de rétention en jours des fichiers de compte-rendus  |
    | SH_RETENTION_LOG   | Vaut 8 par défaut. Duree de rétention en jours des fichiers de traces         |
    | SH_TMP_DIR         | Le nom du répertoire temporaire pour la session ($NC_EXPL_TMP/$SH_SESSION_ID) |
    +--------------------+-------------------------------------------------------------------------------+



Remarques :
* Les variables **SH\_SILENCE** et **SH\_NIV\_DEBUG** sont controlées par les options "**-s**" et "**-dbg**"
si la gestion des options a été activée.
* Le répertoire SH\_TMP\_DIR est systématiquement créé au début du script la bibliothèque params.lib.
* Le répertoire SH\_TMP\_DIR est également détruit à la fin du script (voir erreur.lib).

## III - Utilisation du socle bash 
 
Cette partie décrit sommairement les quelques règles d'usage pour écrire un script Bash avec le socle.

### 1 - Cartouche

Les scripts doivent commencer par le cartouche ci-dessous :

    +----------------------------------------------------------------------------------------------------+
    |                                                                                                    |
    |       #!/bin/bash                                                                                  |
    |       ##----------------------------------------------------------------------------               |
    |       ## Script         : <Nom_du_script>                                                          |
    |       ## Module Puppet  : <Nom_du_module_hebergeant_le_script>                                     |
    |       ## Auteur         : <Auteur ayant cree le script>                                            |
    |       ## Date           : <Derniere_date_d_actualisation_au_format_JJ-MM-AAAA>                     |
    |       ## Version        : <Version>                                                                |
    |       ## Objet          : <Resume de la fonction>                                                  |
    |       ##                                                                                           |
    |       # ----------------------------------------------------------------------------               |
    |       # Mise a jour :                                                                              |
    |       # <Version>  <Date>   - <Auteur>           - <Action> : <Description>                        |
    |       # 0.0.1  08-06-2016   - Toto Laricot       - Cre : Creation                                  |
    |       # 0.0.2  08-09-2016   - Titi Alapeti       - Ano : Ce truc est faux                          |
    |       # ----------------------------------------------------------------------------               |
    |       # Dependance :                                                                               |
    |       # @DEP: <Nom_du_script>:<Version[-|+]>                                                       |
    |       # ----------------------------------------------------------------------------               |
    |       # Liste des options et arguments                                                             |
    |       # @OPT: <type>:<nom>:<var>:<nb_arg>:<def_val>:<OptE>:<OptI>:<ctl_val>  # <Commentaire>       |
    |       # @OPT: <type>:...                                                     # <Commentaire>       |
    |       # @ARG: <type>:<var>:<nb_arg>:<def_val>:<ctl_val>                      # <Commentaire>       |
    |       ##----------------------------------------------------------------------------               |
    |       ##                                                                                           |
    |       ## Syntaxes :                                                                                |
    |       ## --------                                                                                  |
    |       ##                                                                                           |
    |       ##     prompt> <Nom_du_script> <Liste_des_arguments_et_options>                              |
    |       ##                                                                                           |
    |       ##             <Opt1> : <Description>                                                        |
    |       ##                                                                                           |
    |       ## Fonctionnement :                                                                          |
    |       ## --------------                                                                            |
    |       ##                                                                                           |
    |       ##     <Que fait le script dans les details et comment ca marche......>                      |
    |       ##                                                                                           |
    |       ##----------------------------------------------------------------------------               |
    +----------------------------------------------------------------------------------------------------+



**Note:** Les lignes commençant par 2 "#" sont utilisées pour générer l'aide, leur maintient est donc nécéssaire 
pour garantir la fiabilité de l'information. Ce sont ces lignes qui sont lues lors de l'utilisation de 
l'option par defaut "**-h**".
  
#### 1.1 Elements du cartouche

##### 1.1.1 Meta-données

* **&lt;Action&gt;**: peut prendre les valeurs suivantes:

        cre  : Création du script. 
        evo  : Évolution du script -> Ajout de nouvelles fonctionnalités, Amélioration du code. 
        ano  : Correction du code suite à un comportement inattendu. 
        typo : Ajout de commentaires, de texte ou corrections gramaticales sans influence sur le code. 
               
* **&lt;Auteur&gt;**: &lt;Prenom&gt; &lt;Nom&gt;

* **&lt;Version&gt;**; : Format x.y.z
Pour le suivi des versions on utilisera le versionnement sémantique (voir paragraphe III-1.2).

##### 1.1.2 Paragraphe

* **Mise à jour**: Ce paragraphe est utilisé pour suivre l'evolution des versions. La dernière version en
vigueur doit être reportée dans le premier paragraphe.

* **Dépendance**: Permet de définir la liste ainsi que la version des scripts qui sont appelés.
Dans le cas d'un changement cela permet de connaître les scripts qui sont impactés.
Pour le moment aucun script n'exploite cette méta-donnée qui est presente pour un future usage.

* **Liste des options**: La déclaration des options n'est pas obligatoire.
                      
Si aucune déclaration n'est présente alors la bibliothèque params.lib ne fait aucune action.
Dans ce cas les options par défaut **-h**, **-log**, **-hist**, **-cpt**, **-s** et **-dbg** ne
seront pas disponibles.

Si le script ne nécessite pas d'option mais que l'on souhaite disposer des options par défaut, il
faudra déclarer une option bidon.

    # @OPT: f:bidon::::::

Si la déclaration existe alors le socle gèrera les paramètres en entrée du script, ainsi que
les options par défaut **-h**, **-log**, **-hist**, **-cpt**, **-s** et **-dbg**.

**Ces mots clé sont reservés et ne peuvent pas être utilisés par le script.**

La déclaration est réalisée dans le paragraphe "Liste des options et arguments". Chaque
option est identifiée par l'entête de ligne "# @OPT:" suivi de la description.

La description consiste en une liste de paramètres séparés par ":"

        <type>:<nom>:<var>:<nb_arg>:<def_val>:<OptE>:<OptI>:<ctl_val>
        
L'ordre des paramètres est figé et tous doivent etre présents.

**8 champs sont obligatoires**, soit 7 caractères ":" (on ne compte pas le caracatère ":" suivant @OPT).

Exemple de déclaration valide :

        # @OPT: fb:h::0::: 
        # @OPT: fb:s::0:::
        # @OPT: f:dbg:SH_NIV_DEBUG:0/1:::
        # @OPT: fb:log::0:::
        # @OPT: f:hist:SH_ARG_HIST:0/1:10::
        # @OPT: oM:i:G_INSTANCE:1/1:ALL:dbid:
        # @OPT: oM:pr:G_PR_NOM:1/1:sq,t,l:

  * **&lt;type&gt;**: Propriété de l'attribu défini par l'aggrégation des lettres suivantes:
  
            f    -> Option falcultative
            b    -> Booléen
            o    -> Option obligatoire
            M    -> Argument transformé en majuscule


  * **&lt;nom&gt;**: nom de l'option (sans le tiret). Attention le nom de l'option est sensible à la casse.

  * **&lt;var&gt;**: nom de la variable dans laquelle sera stockée le(s) argument(s)
si plusieurs arguments alors un tableau sera initialisé **&lt;var[n]&gt;**
L'index [n] commence à zero. 
Le nombre d'arguments effectivement passés à la commande est **${#&lt;var&gt;[\*]}**.
La valeur max de l'index [n] est **${#&lt;var&gt;[\*]} - 1**.

  * **&lt;nb\_arg&gt;**: nombre d'arguments attendus de la forme "**min/max**", "**min/**", "**max**" ou ""
si vide ou "0" alors l'option est booléene.
la forme "**min/**" considère le nombre max non determiné (infini)

  * **&lt;def\_val&gt;**: Valeur par défaut assignée à **&lt;var&gt;**
Fonctionne si **&lt;nb\_arg&gt;** vaut 0/1 1/1

  * **&lt;OptE&gt;**: Liste des options mutellement exclusives séparées par une virgule.

  * **&lt;OptI&gt;**: Liste des options inclusives séparées par une virgule.

Lors de l'initialisation les marqueurs "**@OPT**" sont lues et analysées.
Le résultat est comparé aux arguments et options sousmis au script et les variables suivantes sont initialisées ...

  * **SH\_OPTION\_&lt;nom&gt;**: Initialisée à "1" (Vrai) ou "0" (Faux) selon que l'option est respectivement utilisée ou non.
Il existe toujours une variable par option déclarée. Ceci concerne aussi les options par défaut gérées par la bibliothèque **params.lib**.

  * **SH\_ARG\_&lt;nom&gt;**: Cette variable existe uniquement si le champ **&lt;var&gt;** est null et si le type n'est pas booléen.
Par défaut elle est initialisée à **null** ou avec la valeur **&lt;def\_val&gt;**
Si le champs **&lt;var&gt;** est renseigné, alors **SH\_ARG\_&lt;nom&gt;** est remplacée pas **&lt;var&gt;**.
Si l'option associée peut prendre plusieurs arguments, alors les variables **SH\_ARG\_&lt;nom&gt;** ou **&lt;var&gt;**
sont des tableaux dont l'index commence a zéro. La taille du tableau peut etre récuperée via
**${#SH\_ARG\_&lt;nom&gt;[\*]}** ou **${#&lt;var&gt;[\*]}**.

Les options **-h**, **-s**, **-log [id]**, **-hist [N]** et **-dbg N** sont prises en charge automatiquement. Ces noms sont donc reservées.

Les options par défaut réalisent les actions suivantes :

    -h        : Affiche l'aide du script (Ligne du cartouche qui commence par "##")
    -s        : Mode silence. Plus d'affichage sur le terminal. 
                Initialise la variable SH_SILENCE a "oui"
    -log [id] : Affiche le dernier journal ou le journal "id" (identifiant de session d'exécution).
    -cpt [id] : Affiche le dernier compte-rendu ou le Compte-rendu "id" (identifiant de session d'execution).
    -hist [N] : Affiche l'historique des "N" dernières executions (N=10 par défaut)
    -dbg N    : Activation du mode debug au niveau "N". Active les messages gérés par la bibliothèque fct_message
                dont l'option -debug est utilisée.


* **Arguments**: La déclaration des arguments du script n'est pas obligatoire

**Attention: Si aucune option "@OPT:" n'est déclarée alors il n y aura pas de prise en charge des arguments.**

Si votre script ne nécessite pas d'option, mais qu'il presente des arguments, il faudra alors déclarer une
option bidon :

    # @OPT: f:bidon::::::

La déclaration est réalisé dans le paragraphe "**Liste des options et arguments**".
Elle est identifiée par l'entête de ligne "**# @ARG:**" suivi de la description.

Il ne peut y avoir qu'une ligne de description pour les arguments. (Si plusieurs lignes avec le
marqueur **@ARG** existent, alors seule la premiere est prise en compte).

La description consiste en une liste de paramètres séparés par ":"

        <type>:<var>:<nb_arg>:<def_val>:<ctl_val>

L'ordre des paramètres est figé et tous doivent être présents.

  * **&lt;type&gt;**: Propriété de l'attribu défini par l'aggrégation des lettres suivantes:

            f    -> Argument falcultative
            o    -> Argument obligatoire
            M    -> Argument transforme en majuscule

  * **&lt;var&gt;**: nom de la variable dans laquelle sera stockée le(s) argument(s)
si plusieurs arguments alors un tableau sera initialisé **&lt;var[n]&gt;**
L'index [n] commence à zero. 
Le nombre d'arguments effectivement passés à la commande est **${#&lt;var&gt;[\*]}**.
La valeur max de l'index [n] est **${#&lt;var&gt;[\*]} - 1**.

Si **&lt;var&gt;** n'est pas renseigné alors la variable **SH\_ARGS[]** est initialisée

  * **&lt;nb\_arg&gt;**: nombre d'arguments attendus de la forme "**min/max**", "**min/**", "**max**" ou "".
si vide alors on considère que le nombre d'arguments est a minima de 1.
la forme "**min/**" considère le nombre est a minima de "**min**".

  * **&lt;def\_val&gt;**: Valeur par défaut assignée à **&lt;var&gt;**
Fonctionne si **&lt;nb\_arg&gt;** vaut 0/1 1/1

**Attention: Si le caractère "**&**" est présent dans la valeur d'un argument, alors cette valeur doit être encadrée 
par une simple quote et le caractère "&" doit être protégé par un anti-slash "\".**

si le caractère "**&**" n'est pas protégé le shell génèrera un fils qui tentera l'execution d'un script portant
le nom de la valeur de l'argument ! Effet de bord garanti !

#### 1.2 Versionnement sémantique

##### 1.2.1 Usage

L'idée de l'utilisation du versionnement sémantique est de définir clairement les intentions 
d'un changement dans un code pour les clients de ce dernier. Ceci permet de fiabiliser la mise en 
place de dépendances et de déterminer les impacts d'un changement.

##### 1.2.2 Format

Le format utilisé est le suivant:  **X.Y.Z**

* X est l'identifiant de **version majeure**.
* Y est l'identifiant de **version mineure**.
* Z est l'identifiant de **version de correction**.
        
##### 1.2.3 Sémantique

* **Version majeure X**: Il vaut "0" lors du développement, le script est considéré non valide et ne 
devrait pas être appelé par d'autres scripts. Une fois le script valide la version doit etre 1.0.0
(première version stable).

Il doit etre incrementé si des changements dans le code n'assure plus la compatibilité ascendante.
Les identifiants de version mineure et de correction doivent etre remis à zero quand l'identifiant
de version majeure est incrementé.

* **Version mineure Y**: Doit être incrementé lors de l'ajout de nouvelles fonctionnalités ou d'amélioration
du code qui n'ont pas d'impact sur la compatibilité ascendante. L'identifiant de version de correction doit
être remis a zero lorsque l'identifiant de version mineure est incrementé.

* **Version de correction Z**: Doit être incrementé si seules des corrections rétro-compatibles sont introduites. 
Une correction est définie comme un changement interne qui corrige un comportement incorrect.
Il peut être incrementé lors de correction typographique ou grammaticale.

    
### 2 - Initialisation du socle

Par défaut les bibliothèques du socle sont chargées à la connexion. Mais pour qu'elles puissent être operationnelles
il faut initialiser les variables de session.

Ceci est réalisé en sourcant le fichier "**${NC\_EXPL\_CFG}/init.conf**" au debut du script :

Remarque: L'analyse des options et arguments est aussi réalisée par "**${NC\_EXPL\_CFG}/init.conf**".

    +----------------------------------------------------------------------------------------------------+
    |                                                                                                    |
    |       #!/bin/bash                                                                                  |
    |       ##----------------------------------------------------------------------------               |
    |       .                                                                                            |
    |                                   <CARTOUCHE>                                                      |
    |       .                                                                                            |
    |       ##----------------------------------------------------------------------------               |
    |       #-----------------------------------------------------------------------------               |
    |       #                 Initialisation de l environement                                           |
    |       #-----------------------------------------------------------------------------               |
    |       if [ ! -f ${NC_EXPL_CFG}/init.conf ]                                                         |
    |       then                                                                                         |
    |           echo "Fichier d initialisation du socle ${NC_EXPL_CFG}/init.conf n existe pas !"         |
    |           echo "Arret du script par securite"                                                      |
    |           exit 250                                                                                 |
    |       else                                                                                         |
    |           . ${NC_EXPL_CFG}/init.conf                                                               |
    |       fi                                                                                           |
    +----------------------------------------------------------------------------------------------------+



### 3 - Structure d'un script

Voici les éléments qui devraient être présents a minima dans les script Bash. Ceci est recommandé pour
assurer une homogénéite dans l'ensemble du socle.
                                                                                    
    #!/bin/bash                                                                                  
    ##----------------------------------------------------------------------------               
    .                                                                                            
                               <CARTOUCHE>                                                      
    .                                                                                            
    ##----------------------------------------------------------------------------               
    #-----------------------------------------------------------------------------               
    #                 Initialisation de l environement                                           
    #-----------------------------------------------------------------------------               
    if [ ! -f ${NC_EXPL_CFG}/init.conf ]                                                         
    then                                                                                         
       echo "Fichier d initialisation du socle ${NC_EXPL_CFG}/init.conf n existe pas !"         
       echo "Arret du script par securite"                                                      
       exit 250                                                                                 
    else                                                                                         
       . ${NC_EXPL_CFG}/init.conf                                                               
    fi                                                                                           
                                                                                                
    #-----------------------------------------------------------------------------               
    #                          Definition des fonctions                                          
    #-----------------------------------------------------------------------------               
    #                                                                                            
    function fct_un                                                                              
    { ...                                                                                        
    }                                                                                            
                                                                                                
    #-----------------------------------------------------------------------------               
    function fct_deux                                                                            
    { ...                                                                                        
    }                                                                                            
                                                                                                
    #-----------------------------------------------------------------------------               
    #                             Programme principal                         MAIN               
    #-----------------------------------------------------------------------------               
    #                                                                                            
    fct_message "${SH_PROG}"                                                                     
    fct_message "version ${SH_PROG_VERSION}"                                                     
                                                                                                
    # -- Initialisation des variables par defaut                                                 
    # -- ---------------------------------------                                                 
    G_VAR1=${G_AVR1:="Valeur par defaut"}           # <Comentaire>                               
    ...                                                                                          
                                                                                                
    # -- Contrôle de cohérence des options                                                        
    # -- --------------------------------                                                        
                                                                                                
    # -- Traitement                                                                              
    # -- ----------                                                                              
                                                                                                
    ...                                                                                          
                                                                                                
    fct_erreur 0         


**Note**: La construction **G\_VAR1=${G\_AVR1:="Valeur par défaut"}** permet de définir une variable avec une 
valeur par défaut, qui peut être surchargée dans le fichier de configuration **${NC\_EXPL\_CFG}/&lt;Nom\_du\_script&gt;.conf**

Concernant les fonctions **fct\_message** et **fct\_erreur** voir le chapitre V.


### 4 - Actions réalisées au lancement d'un script

Le script commence par le chargement de la configuration "**. ${NC\_EXPL\_CFG}/init.conf**"
un certain nombre d'actions sont effectuées par défaut en plus de l'initialisation des variables de session.

* Initialisation des variables de session (Cf. II.3)
* Génération d'un numéro de session unique sous la varable **${SH\_SESSION\_ID}**
* Chargement du fichier **${SH\_FICCFG}**
* Rotation des fichiers **${SH\_FICLOG}** et **${SH\_FICCPT}** selon les rétentions respective **${SH\_RETENTION\_LOG}** et **${SH\_RETENTION\_CPT}**
* Purge des fichiers temporaires (${NC\_EXP\_ROOT}/tmp) antérieurs selon la retentions **${SH\_RETENTION\_LOG}** et
suppression des fichiers générés par le script utilsant le socle dans **${NC\_EXP\_ROOT}/log** et **${NC\_EXP\_ROOT}/cpt**
* Ecriture dans le fichier **${SH\_FICTRC}** de la date de début exécution ainsi que les arguments passés au script
* Initialisation du fichier **${SH\_FICLOG}** à vide
* Analyse des paramètres passés au script

Cette analyse ne fonctionne que si les options ont été déclarées dans l'entête du script (Cf. III.1.1.2)



## IV - Nomenclature

### 1 - Fichiers temporaires

Les fichiers temporaires doivent etre écrits dans le répertoire **${NC\_EXPL\_TMP}**.
Les noms des fichiers temporaires sont de la forme : **&lt;Libre&gt;\_${SH\_SESSION\_ID}.tmp**
De cette facon il seront gérés par le socle (Suppresion automatique).

### 2 - Nom des exécutables (fichiers du dossier **${EXPL\_ROOT}/bin** )

Les noms des fichiers doivent être de la forme :

    <pre>_<descr>
    <pre>_<descr>.<language>
    <pre>_<app_source>_<app_dest>[_<libre>].<language>
    <app>_<descr>.<language>


* **&lt;pre&gt;**: est une des valeurs suivantes : "**sys**", "**dba**", "**exp**", "**ctl**", "**aud**", "**int**"
  * "**sys**": Scrits d'administration système.
  * "**dba**": Scripts d'administration base de données
Ces scripts peuvent présenter des risques et doivent faire l'objet d'une attention particulière
lors de leurs exécution.
  * "**exp**" : Scripts d'exploitation dévolus principalement aux traitements réccurents
Ces scripts sont réservés à l'exploitation courante. Il peuvent modifier le système dans le cadres de
traitements réccurents.
Leurs exécution est de risque modére, comme respecter une date d'execution.
Il sont reservés à l'exploitation système uniquement.
Dans le cas d'un traitement réccurrent pour le compte d'une application le nom doit
être de la forme <app>\_<descr>.<language>
  * "**int**" : Scripts dédiés aux interfaces entre applications
Ces scripts doivent faire l'objet d'une attention particulière car il peuvent modifier
les données applicatives et peuvent être contraints par des conditions d'execution.
  * "**ctl**" : Script de contrôle (dévolu principalement au système de monitoring)
  * "**aud**" : Script d'audit ne modifiant pas le système.
Ces scripts peuvent être executés sans aucun risque.

* **&lt;descr&gt;** est libre, doit refléter si possible la fonction du script.

* **&lt;language&gt;** est une des valeurs suivantes :

    "**sh**" : Script Bash.
    
* **&lt;app&gt;** est le nom de l'application à laquelle le script est dédié.
Ces scripts sont réservés à une application particulière dans le cadres de l'exploitation
réccurente de l'application.


## V - Description et usage des bibliothèques

### 1 - fct\_message

* **Syntaxe**: fct\_message [-cpt] [-hist] [-nolog] [-debug N] [-color Couleur] "&lt;Message à ecrire&gt;"

* **Usage**: Cette fonction est à utiliser en remplacement de la commande "echo".
Elle ajoute l'horadatage au message et gère la ventilation des écritures dans les differents fichiers de traces.

* **Fonction**: Par défaut le message est affiché sur le terminal et est écrit dans le fichier journal **${NC\_EXPL\_LOG}/&lt;nom\_du\_script&gt;.log**

Les options modifient la destination de l'écriture :

  * **-cpt**: Écrit en plus du journal dans le fichier de compte-rendu **${NC\_EXPL\_CPT}/&lt;nom\_du\_script&gt;.cpt**
  * **-hist**: Écrit uniquement dans le fichier d'historique des exécutions **${NC\_EXPL\_TRC}/&lt;nom\_du\_script&gt;.hist**
  * **-nolog**: Inhibe l'écritutre dans le fichier journal
  * **-debug N**: N'affiche pas le message sur le terminal mais uniquement dans le fichier journal, à condition
  que le script soit execute en mode debug de niveau equivalent à "N" via l'option "**-dbg N**".
  Par défaut le niveau de debug est 0, ainsi si "N" vaut zero alors le message est écrit uniquement dans le fichier journal.
  * **-color**: Affiche le message en couleur. Les couleurs reconues sont : vert, rouge, bleu, cyan, jaune, orange, blanc et noir


### 2 - fct\_affiche\_ligne

* **Syntaxe**: fct\_affiche\_ligne [entete|pied] &lt;elements&gt;
* **Usage**: Affiche un tableau sur la sortie standard et dans le fichier compte-rendu **${NC\_EXPL\_ROOT}/&lt;script&gt;.cpt**
* **Fonction**: 3 étapes sont nécéssaires pour créer un tableau :
  * Définition du tableau
  * Affichage des éléments
  * Fermeture du tableau

Ces étapes doivent impérativement être suivies et dans cet ordre.

**Il n'est pas possible d'imbriquer des tableaux.**

* **Définition**: Elle est réalisée par l'appel de la fonction **fct\_affiche\_ligne** avec l'argument entête. La forme est alors la suivante : 

        fct_affiche_ligne entete "{Titre}" "{Colonne_1},{Taille_1}" "{Colonne_2},{Taille_2}" "{Colonne_3},{Taille_3}" "{Colonne_4},{Taille_4}" ...

  ou bien:

          fct_affiche_ligne entete "{Titre}" "Groupe1({Colonne_1},{Taille_1}:{Colonne_2},{Taille_2})" "{Colonne_3},{Taille_3}" "{Colonne_4},{Taille_4}" "Groupe2({Colonne_4},{Taille_4}:...)" 


* Affichage des éléments:

        fct_affiche_ligne "<Element1>" "<Element2>" ... 

* Fermeture du tableau: Cette action permet de libérer les variables de session qui ont été créées

        fct_affiche_ligne pied


* Exemples:

        fct_affiche_ligne entete "Un tableau" "C1,10" "C2,10" "c3,10"
        fct_affiche_ligne A B C
        fct_affiche_ligne 1 "" 3
        fct_affiche_ligne pied

  affichera:

        --------------------------------------
                     Un tableau
        --------------------------------------
             C1     |     C2     |     c3
        --------------------------------------
        A           |B           |C
        1           |            |3
        --------------------------------------

  et 

        fct_affiche_ligne entete "Un Autre tableau" "C1,10" "G1(C2,20:C3,10:C4,3)" "C5,15"
        fct_affiche_ligne A B C D E
        fct_affiche_ligne 1 "" 3 3 4
        fct_affiche_ligne pied

  affichera:
                    
        ------------------------------------------------------------------------
                                    Un Autre tableau
        ------------------------------------------------------------------------
                    |                   G1                    |
             C1     |-----------------------------------------|       C5
                    |          C2          |     C3     | C4  |
        ------------------------------------------------------------------------
        A           |B                     |C           |D    |E                
        1           |                      |3           |3    |4                
        ------------------------------------------------------------------------


### 3 - fct\_erreur

* **Syntaxe**: fct\_erreur &lt;N&gt;
* **Usage**: Gestion des erreurs d'un script
* **Fonction**: Cette fonction doit etre appelée pour sortir d'un script du socle.
Elle affiche le message d'erreur associé au code qui est lu dans le fichier **${NC\_EXPL\_MSG}/&lt;script&gt;.msg**
Elle exécute éventuellement une fonction rattachée, spécifiée dans ce même fichier.
Elle ferme les fichiers de traces, supprime les fichiers temporaires, libère les semaphores et 
renvoie un code retour au script appelant.

N est un entier positif dont les valeurs suivantes sont reservées :

    0   : Fin normale du script
    250 : <lib:fct_erreur> Appel de la bibliothèque sans le numero d'erreur (<N> non renseigne !)
    251 : <lib:fct_erreur> La description de l'erreur <N> n'existe pas dans le fichier <script>.msg
    252 : <lib:fct_erreur> Le fichier <script>.msg n'existe pas
    253 : <lib:fct_erreur> La description de l'erreur dans le fichier <script>.msg est incorrecte
    254 : <lib:fct_params> Erreur a été remontée par la bibliothèque 
    255 : <lib:fct_erreur> Appel de la bibliothèque avec un numero d'erreur >= 250
    256 : réservé pour un futur usage



* Le fichier **&lt;script&gt;.msg**: Contient les descriptions des codes erreur. 
  * Un code par ligne de la forme:

              <NumErreur> | <type> | <action> | <message> ou <fonction>


    * **&lt;NumErreur&gt;**: Entier positif identifiant l'erreur
    * **&lt;type&gt;**: prend les valeurs erreur, alert ou info. Colorise le message en fonction
    du type respectivement rouge, orange et bleu 
    * **&lt;action&gt;**: Action à realiser (continu, stop, exec)
      * **continu**: Affiche le message puis rend la main au script
      * **stop**: Affiche le message puis sort en erreur
      * **exec**: exécute la fonction puis rend la main au script
    * **&lt;fonction&gt;**: Fonction à exécuter. Ce champs peut contenir des variables qui seront valorisées.
    * **&lt;message&gt;**: Message à afficher (dans les traces et sur la sortie standard). Ce champs peut contenir
    des variables qui seront valorisées.

### 4 - fct\_unique\_exec


* **Syntaxe**: fct\_unique\_exec [&lt;nom&gt;] [&lt;Occurences&gt;]
* **Usage**: Empeche l'exécution multiple d'un code par l'acquisition de sémaphore
* **Fonction**: [DOCUMENTATION MANQUANTE]


### 5 - fct\_libere\_exec

* **Syntaxe**: fct\_libere\_exec [&lt;nom&gt;]
* **Usage**: Libère les sémaphores
* **Fonction**: [DOCUMENTATION MANQUANTE]


### 6 - fct\_usage

* **Syntaxe**: fct\_usage
* **Usage**: Affiche l'aide du script. Ce sont les lignes du cartouche commencant par "##"
* **Fonction**: Cette fonction est uniquement à usage interne du socle. Elle est intégrée à la fonction fct\_params.

### 7 - fct\_rotation

* **Syntaxe**: [DOCUMENTATION MANQUANTE] 
* **Usage**: [DOCUMENTATION MANQUANTE]
* **Fonction**: Cette fonction est uniquement à usage interne du socle


### 8 - fctpath

* **Syntaxe**: [DOCUMENTATION MANQUANTE] 
* **Usage**: [DOCUMENTATION MANQUANTE]
* **Fonction**: Cette fonction est uniquement à usage interne du socle

### 9 - fct\_params

* **Syntaxe**: fct\_params
* **Usage**: Gestion des options et arguments du script
* **Fonction**: Cette fonction est uniquement à usage interne du socle.

Elle est appelée à l'initialisation des variables de session lors du chargement du fichier **${NC\_EXPL\_CFG}/init.conf**

Cette fonction extrait les lignes de déclaration "# OPT:" et les compare aux arguments sousmis au script.

Pour plus d'information voir le paragraphe III-1.1.2  
